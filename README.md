# iCEBreaker-FPGA

## what is an FPGA?

Computers are made up of a collection of transistors which circuit designers combine to create basic
logic gates (AND, OR, XOR, NOT, NAND, etc.), which are then built in to more complex computing
modules (such as Adders, Arithmetic Logic Units (ALUs), or Floating Point Units (FPUs)). These
modules are finally combined into an overarching computer architecture which is exposed to
applications/programmers. For example, below is the ARM6 core architecture which is used in the
SAMD11 microchip:

<img src="./imgs/arm6_core.png" height="500">

[ARM6 Microarchitecture](https://en.wikichip.org/wiki/arm_holdings/microarchitectures/arm6#:~:text=ARM6%20is%20an%20ARM%20microarchitecture,spun%2Doff%20from%20Acorn%20Computers.)

When programming a microcontroller like the SAMD11, you can control what instructions are passed to
the control unit (and thus what information is written and read from memory/registers) by changing
the software flashed on the processor's memory; however, you have no control over the underlying
hardware.

FPGA stands for **"Field Programmable Gate Array"** - it is a collection of integrated circuit logic
gates and interconnects which can be programmed and reconfigured directly by the developer. This is
very different from any other type of computer or microcontroller because you now have control over
the hardware and can define custom circuits.

<img src="./imgs/fpga_architecture.png" height="500"> \

[intro to FPGA architecture](https://towardsdatascience.com/introduction-to-fpga-and-its-architecture-20a62c14421c)

## programming FPGAs (Verilog)

To program an FPGA, you use a hardware description langauge, here we'll focus on
[Verilog](https://www.verilog.com/), but [VHDL](https://en.wikipedia.org/wiki/VHDL) is also commonly
used and simiar. Hardware Description langauges can be thought of as a markup langauge for circuit
layout - basically you're only one abstraction layer above defining gate level logic. Keep in mind
that unlike programming languages they don't define sequences of instructions - they define
schematics. This changes the philosphy for writing algorithms and applications in meaningful ways,
most noteably:

- All statements are executed concurrently. Unlike a simple C program that will be executed line by
  line inside the main() block, each line in verilog (except those between begin and end statements)
  is executed at the same time.

- Verilog code is synthesized and then everything is mapped to hardware gates; as opposed to other
  langauges which are compiled and stored somewhere that the CPU may or may not access.

Verilog code is built up of modules, which have defined inputs and outputs (they can be thought to
bee similar to functions), which execute combinatorial logic and/or pass values between registers.
For example, a full 1-bit adder in verilog looks like this

```
module full_adder(
    input in_bit_1,
    input in_bit_2,
    input in_carry,
    output out_sum,
    output out_carry
    );
  wire   w_WIRE_1;
  wire   w_WIRE_2;
  wire   w_WIRE_3;

  assign w_WIRE_1 = i_bit1 ^ i_bit2;
  assign w_WIRE_2 = w_WIRE_1 & i_carry;
  assign w_WIRE_3 = i_bit1 & i_bit2;

  assign o_sum   = w_WIRE_1 ^ i_carry;
  assign o_carry = w_WIRE_2 | w_WIRE_3;
endmodule
```

which is just a written implementation of a full adder logic circuit.

<img src="./imgs/full_adder.png" height="500">

## programming toolchain

Unlike programming a microcontroller (code is _compiled_ --> a binary file is generated -> then that binary data is stored into the flash memory of the chip), verilog needs to be _synthesized_:

1. The synthesis tool expands the verilog code into a gate level netlist following rules given by some predefined library. [Yosys](http://www.clifford.at/yosys/) is a commonly used open-source RTL synthesis tool for this

2. Then the netlist is passed to a "place and route" (pnr) tool which grounds the netlist in the physcial world by figuring out the placement and routing between logic gates. [nextpnr](https://github.com/YosysHQ/nextpnr) pairs well with yosys for this.

3. Finally, the output of the pnr tool is passed to the FPGA hardware as a bistream and the physical hardware is modified.

## stopwatch hello-world

The iCEBreaker FPGA breakout board is a great platform for hello-worlds beginner FPGA development.
Refer to the [icebreaker-fpga-workshop](https://github.com/icebreaker-fpga/icebreaker-workshop) for
step by step instructions on installing the necessary tools and building a FPGA stopwatch.

## CBA hello worlds

See the [code](code) directory.

## why??

FPGAs have been growing in popularity, especially among hobbyists and academics, over the last 5 -
10 years as open source development tools have become more accessible and custom hardware
optimizations become more important for ultra-low power embedded applications. FPGA programming has
a wide range of applications

- accelerated machine learning algorithms
  - image / video processing
- high speed data acquisition at the edge (e.g. particle accelerators)
- easily parallelized communication tasks
- ASIC / architecture prototyping
  - [opencores](https://opencores.org/)
- basically anything with vertical development (limited in quantity with highly specialzed task)
